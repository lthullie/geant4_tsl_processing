#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import os
from Utility import *
from PENDFFileReader import *

class PENDFFile_MF6_elastic_incoherent(PENDFFileReader):
	def __init__(self, file_in_path, file_out_path, temperature):
		#print('PENDFFile_MF6_elastic_incoherent: file_in=', file_in_path)
		PENDFFileReader.__init__(self, file_in_path, file_out_path+'_tmp')
		#-------------------------------------------------------------
		# Read and transform PENDF data to G4NDL data
		#-------------------------------------------------------------       

		print('file_in_path=', file_in_path)
		print('file_out_path=', file_out_path)
 
		items = self.readLine() #header 1
		self.ZA = items[0]
		self.AWR = items[1]
		self.MF = items[7]
		self.MT = items[8]
		items = self.readLine() #header 2
		items = self.readLine() #header 3
		items = self.readLine() #header 4
		self.Emin = items[0]
		self.Emax = items[2]
		items = self.readLine() #header 5
		self.temperature = items[0]
		items = self.readLine() #header 6
		self.gridE_size = 1 
		BLOCK_SIZE = 0
		number_pair_of_lines = 0

		for i in range(self.gridE_size):
			iteration = 0
			
			line  = self.file.readline()
			while line != '' :
				# First line is to write complete
				items = self.splitLine(line)

				Emin = items[0]
				Emax = items[1]
				block_size = int(items[5])
				BLOCK_SIZE = block_size
				block_size_tmp = 0
				data_line = []
				line = get_line_from_list_6char_without_EOL(items) + ' \n'
				self.file_out.write(line)
				isRunLoop = True
				while isRunLoop:
					line  = self.file.readline()
					items = self.splitLine(line)
					
					for j in range(6):
						data_line.append(items[j])
						block_size_tmp += 1
						
						if block_size_tmp==block_size:
							block_size_tmp = 0
							#print('data_line=', data_line)
							line_w = get_line_from_list_without_EOL(data_line) + '\n'
							self.file_out.write(line_w)
							number_pair_of_lines += 1
							data_line = []
							isRunLoop = False # Break the while loop  
							break
				#Next line
				line  = self.file.readline()

		self.file_out.close()
        
        #-------------------------------------------------------------
        # Write GNDL file
        #-------------------------------------------------------------
		print('-----------------------------------------')
		print('----PENDFFile_MF6_elastic_incoherent-----')
		print('-----------------------------------------')
		file_in = open(file_out_path+'_tmp', 'r')
		file_out = open(file_out_path, 'w')
		#file_out.write('{:<11}'.format('G4NDL')+'\n')
		#file_out.write('{:>11}'.format(ENDF_LIBRARY)+'\n')
		file_out.write(formatString(int(self.MF))+'\n') 
		file_out.write(formatString(int(self.MT))+'\n') 
		file_out.write(formatString(temperature)+'\n') 
		file_out.write(formatString(int(number_pair_of_lines))+'\n') #Number of pair of lines to process
		file_out.close()
		file_out = open(file_out_path, 'a')
		line_nb = 0
		
		for line in file_in:
			i_start = 0
			i_step = 13
			items = []
			if line_nb == 0:
				for i in range(6):
					chunk = line[i_start:i_start+i_step]
					#print('--> 1/ chunk=', chunk)
					items.append( string_to_float(chunk) ) #values
					i_start += i_step
				data_tmp = [0, items[1], 0, 0, int(items[4]), int(items[5])]
				#print('1/ data_tmp=', data_tmp)
				line = get_line_from_list_without_EOL(data_tmp) + '\n'
				file_out.write( line )
			elif line_nb == 1:
				#print('line=', line)
				for i in range(BLOCK_SIZE):
					chunk = line[i_start:i_start+i_step]
					#print('--> 2/ chunk=', chunk)
					items.append( string_to_float(chunk) ) #values
					i_start += i_step
				#if items[1] == 0.005:
				#	input()
			
				items2 = [items]
				#items3 = np.nan_to_num(items2, nan=0, posinf=33333333, neginf=33333333)
				np.savetxt(file_out, items2, fmt='%.6e')
			
			line_nb += 1
			if line_nb == 2:
				line_nb = 0

		file_in.close()
		file_out.close()
		os.remove(file_out_path+'_tmp') #delete intermediate file
        
        
