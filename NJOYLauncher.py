#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import os
import subprocess
from Utility import *
from ENDFFileReader import *


def NJOYLauncher(NJOY_BINARY_PATH, NEUTRON_XS_DATA_PATH, TSL_DATA_PATH, NJOY_EMAX_TSL, NJOY_ENERGY_TOLERANCE, NJOY_ANGLE_NUMBER, csv_file_row, outputDirectory):
	#******************************************************
	# NJOY launcher
	#******************************************************

	#csv_file = csv.read(ENDF_LIBRARY+ '/file_relation.csv') #names=['TSL', 'Neutron', 'Mtref', 'Natoms'])
	cwd = os.getcwd()

	#Read CSV line
	endf_tsl_file_path = TSL_DATA_PATH + '/' + csv_file_row[0]        #cwd + '/' + ENDF_LIBRARY + '/tsl/' + csv_file_row[0]
	endf_n_file_path   = NEUTRON_XS_DATA_PATH + '/' + csv_file_row[1] #cwd + '/' + ENDF_LIBRARY + '/neutrons/' + csv_file_row[1]

	tape70_path = outputDirectory + '/tape70'
	tape20_path = outputDirectory + '/tape20'
	#Create symbolic link to pass the file to NJOY
	if os.path.exists( tape70_path ):
		os.remove( tape70_path )
	if os.path.exists( tape20_path ):
		os.remove( tape20_path )
	os.symlink(endf_tsl_file_path, tape70_path)
	os.symlink(endf_n_file_path, tape20_path)

	#Read files to extract the options
	n_file = ENDFFileReader(endf_n_file_path)
	tsl_file = ENDFFileReader(endf_tsl_file_path)

	#------------------------------------------------------------
	# Define the options
	#------------------------------------------------------------
	if csv_file_row[2] != '':
		mtref = csv_file_row[2]
	natoms = csv_file_row[3]

	print('tsl_file.is_elastic_coherent=', tsl_file.is_elastic_coherent, ' - tsl_file.is_elastic_incoherent=', tsl_file.is_elastic_incoherent, ' - tsl_file.is_inelastic=', tsl_file.is_inelastic)
	subDirectory_list = []

	#Select option : inelastic coherent
	is_free_gas = False
	iin = '0'
	if tsl_file.is_inelastic:
		iin = '2' 
		subDirectory_list.append('Inelastic')
	elif is_free_gas:
		iin = '1' 
		
	#Select option : elastic coherent
	icoh = '0' # default = none
	if tsl_file.is_elastic_coherent: #compute using ENDF-6 format data
		icoh = '1'
		subDirectory_list.append('Coherent')
	elif tsl_file.is_elastic_incoherent:
		subDirectory_list.append('Incoherent')
	
	#Emax TSL
	emax_tsl = NJOY_EMAX_TSL 				#'4.0' #'4.95'
	#Angular grid binning : #https://www.sciencedirect.com/science/article/abs/pii/S0168900218302079#!
	angle_nb = NJOY_ANGLE_NUMBER 			#'32'     
	#Tolerance regarding the allowed discrepancies between the interpolated Sab and the true Sab: #https://www.sciencedirect.com/science/article/abs/pii/S0168900218302079#!
	tolerance = NJOY_ENERGY_TOLERANCE 		#'0.001 ' 
	tolerance_rec = NJOY_ENERGY_TOLERANCE	#'0.001'
	#Number of atoms in the material
	natoms = csv_file_row[3]
	#Output format
	iform = '0' # 0=E-E'-mu  , 1=E-mu-E'
	#Printing option
	iprint = '0' #0=minimum , 1=maximum , 2=max + iintermediate results

	#Get the temperature list
	temperature_list = csv_file_row[4]
	temperature_nb = int(len(temperature_list.split()))

	#------------------------------------------------------------
	# Create THERMR file to be processed by NJOY
	#------------------------------------------------------------
	thermr_file_path = outputDirectory + '/' + 'thermr.input'
	thermr_file = open(thermr_file_path, 'w')

	#From ACSII to binary for element file
	thermr_file.write('moder \n')
	thermr_file.write('20 -25 \n') # [convert ACSII tape20 to block-binary tape25]

	#From ACSII to binary for TSL file
	thermr_file.write('moder \n')
	thermr_file.write('70 -75 \n') # [convert ACSII tape70 to block-binary tape75]

	# ENERGY GRID RECONSTRUCTION
	thermr_file.write('reconr \n')
	thermr_file.write('-25 -21 \n') # [add point in the energy grid in tape25 and output it in tape21]
	thermr_file.write('\'***** File Name****\' \n') # [Name]
	thermr_file.write(str(n_file.MAT) + ' 0 0 \n') # [Material to be reconstructed, default, default]
	thermr_file.write(tolerance_rec+' 0 '+ str(float(tolerance_rec)*10.) +' 5e-08 \n') # [err , reconstruction temperature default=0K, errmax, errint]
	thermr_file.write('0 /\n') # [ncards of descriptive comments for mt451]

	# BROADENING
	thermr_file.write('broadr \n')
	thermr_file.write('-25 -21 -22 \n')
	thermr_file.write(str(n_file.MAT) + ' ' + str(temperature_nb) +' 0 0 0 \n') #/ [Material, nb of temperature, ...] 
	thermr_file.write('0.001 1e+06 0.01 5e-08 \n') # [tolerance, max energy, fractional tolerance when integral criterion is satisfied, parameter to control integral thinning]
	thermr_file.write(temperature_list + ' \n') # temperature list
	thermr_file.write('0 \n') # [next MAT number to be processed with these parameters. Terminate with mat1=0.]

	# THERMR
	thermr_file.write('thermr \n')
	thermr_file.write('-75 -22 -21 \n') #[endf tape for mf7 data, old pendf tape, new pendf tape]
	thermr_file.write(str(tsl_file.MAT) + ' ' + str(n_file.MAT) + ' ' + angle_nb + ' ' + str(temperature_nb) + ' ' + iin + ' ' + icoh + ' 0  ' + natoms + ' ' + mtref + ' ' + iprint + ' \n') 
	thermr_file.write(temperature_list + ' \n') # temperature
	thermr_file.write(tolerance+' ' + emax_tsl + ' \n') # [tolerance, emax for thermal treatment]

	#From binary to ACSII
	thermr_file.write('moder \n')
	thermr_file.write('-21 30 \n') #[convert back block-binary tape21 to ASCII tape30]
	thermr_file.write('stop')

	thermr_file.close()

	#------------------------------------------------------------
	# Launch NJOY
	#------------------------------------------------------------
	#cmd = ['/local/home/lthullie/Documents/Programs/NJOY/NJOY21/bin/njoy21','<', thermr_file_path ]
	print('********** Start to launch NJOY **********')
	os.chdir(outputDirectory)
	print('CWD=', os.getcwd())
	thermr_launcher_path = outputDirectory + '/thermr_launcher.sh'
	print('---> thermr input file=', thermr_launcher_path)
	thermr_launcher_file = open(thermr_launcher_path, 'w')
	thermr_launcher_file.write('#!/bin/bash \n')
	thermr_launcher_file.write(NJOY_BINARY_PATH+' < '+ os.getcwd()+ '/thermr.input')
	thermr_launcher_file.close() 
	cmd = ['/bin/bash', thermr_launcher_path]
	print('cmd=', cmd)
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	p.wait()
	summary = p.communicate()
	print('NJOY summary=', summary)

	os.chdir(cwd)
	print('********** End of NJOY **********')

	return outputDirectory + '/tape30', subDirectory_list
    
