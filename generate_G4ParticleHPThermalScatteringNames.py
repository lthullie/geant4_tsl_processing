#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import pandas as pd
import os
import csv


def generate_G4ParticleHPThermalScatteringNames(dict_file_path, library, geant4Name, geant4FullName):
	#print('------------ generate_G4ParticleHPThermalScatteringNames - begin ------------')
	#--------------------------------------------------------
	# Create the database if it does not already exist
	#--------------------------------------------------------
	if not os.path.isfile(dict_file_path):
		file = open(dict_file_path, 'w')
		file.write("geant4Name,geant4FullName\n")
		file.close()

	#--------------------------------------------------------
	# Read previous dictionnary status
	#--------------------------------------------------------
	df = pd.read_csv(dict_file_path)
	
	is_in = False
	for idx, row in df.iterrows():
		if row['geant4Name']==geant4Name and row['geant4FullName']==geant4FullName:
			is_in = True
		elif row['geant4Name']!=geant4Name and row['geant4FullName']==geant4FullName:
			print(f"1/ row={row['geant4Name']} - geant4Name={geant4Name} - row={row['geant4FullName']} - geant4FullName={geant4FullName}")
			raise Exception('1/ '+geant4Name+' - '+geant4FullName)
		elif row['geant4Name']==geant4Name and row['geant4FullName']!=geant4FullName:
			print(f"2/ row={row['geant4Name']} - geant4Name={geant4Name} - row={row['geant4FullName']} - geant4FullName={geant4FullName}-")
	 		#raise Exception('2/ '+geant4Name+' - '+geant4FullName)
	 
	
	if not is_in:
		#Add the new material in the database
		dict_tmp = {"geant4Name" : geant4Name, "geant4FullName" : geant4FullName, "library" : library}
		df = df.append(dict_tmp, ignore_index=True)
	else:
		#Add library to this material - ease the user guidance
		idx_list = df.index[df['geant4Name']==geant4Name].tolist()
		if not (library in df.iloc[idx_list[0]]['library']):
			df.iloc[idx_list[0]]['library'] = df.iloc[idx_list[0]]['library'] + ' and ' + library
		
	#--------------------------------------------------------
	# Update Geant4 dictionnary
	#--------------------------------------------------------
	df.to_csv(dict_file_path, index=False)
	
	#--------------------------------------------------------
	# Recreate G4ParticleHPThermalScatteringNames list
	#--------------------------------------------------------
	output_file_path = dict_file_path
	output_file_path = output_file_path.replace('.csv', '.G4ParticleHPThermalScatteringNames.cc.txt')
	if os.path.isfile(output_file_path):
		os.remove(output_file_path)
	file_out = open(output_file_path, 'w')
	
	for idx, row in df.iterrows():
		file_out.write('names.insert ( std::pair < G4String , G4String > ( "'+ row['geant4FullName'] + '", "'+ row['geant4Name'] +'" ) ); \t\t ///'+ row['library'] +' \n')
	
	file_out.close()
	
	#print('------------ generate_G4ParticleHPThermalScatteringNames - end ------------')



#--------------------------------------------------------
# For testing purpose
#--------------------------------------------------------
if __name__ == "__main__":
	csv_file_path_list = ['files_relation_all_jeff33_tsl_endfB8.csv', 'files_relation_all_jeff33_tsl_jeff33.csv', 'files_relation_all_jeff33_tsl_endfB7.csv']
	library_list = ['ENDF/BVIII.0', 'JEFFF.3.3', 'ENDF/BVII.1']
	
	for i in range(len(csv_file_path_list)):
		csv_file_path = csv_file_path_list[i]
		print(f"file={csv_file_path}")
		library = library_list[i]
		
		csv_file = open(csv_file_path, 'r')
		csv_reader = csv.reader(csv_file, delimiter=",") #names=['TSL', 'Neutron', 'Mtref', 'Natoms', 'Temperature', Geant4Name])
		next(csv_reader, None)
		dict_file_path = '../geant4_dict.csv'
		
		for i, csv_file_row in enumerate(csv_reader):
			geant4Name = csv_file_row[5]
			geant4FullName = csv_file_row[6]
			
			generate_G4ParticleHPThermalScatteringNames(dict_file_path, library, geant4Name, geant4FullName)
	
	
