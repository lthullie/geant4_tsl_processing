#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

from Utility import *
from PENDFFileReader import *

class ENDFFileReader(PENDFFileReader):
    def __init__(self, endf_file_in_path):
        PENDFFileReader.__init__(self, endf_file_in_path, 'dummy')
        
        #Get temperature list
        self.temperature_list = []
        self.is_elastic_coherent =  False    #MF=7 - MT=2 - LTHR=1
        self.is_elastic_incoherent =  False  #MF=7 - MT=2 - LTHR=2
        self.is_inelastic =  False           #MF=7 - MT=4
        
        #First line
        self.file.readline() 
        line = self.file.readline() 
        items = self.splitLine(line)
        self.ZA = items[0]
        self.AWR = items[1]
        self.MAT = items[6]
        
        for i in range(2):
            self.file.readline()
        
        #Get the header length
        items = self.readLine()
        self.NWD = int(items[4]) #Number of records with descriptive text for this material. Each record contains up to 66 characters
        
        #Skip the text file
        for i in range(self.NWD):
            line = self.file.readline()
                    
        #Read data classified by MF and MT
        line = self.file.readline()
        MF_tmp = 0
        MT_tmp = 0
        isON = True
        
        while isON and line!='':
            items = self.splitLine(line)
            MF = items[7]
            MT = items[8]
            #print('MF=', MF, ' - MT=', MT, ' - type=', type(MF) )
                        
            if(MF==7 and MT==4):
                print('Inelastic')
                self.is_inelastic =  True
                isON = False
                
            if( MF==7 and MT==2 and MF_tmp!=7 and MT_tmp!=2 ):
                self.LTHR = items[2]
                print('self.LTHR=', self.LTHR)
                if self.LTHR == 1:
                    self.is_elastic_coherent = True
                elif self.LTHR == 2:
                    self.is_elastic_incoherent = True
                
            MF_tmp = MF
            MT_tmp = MT
            line = self.file.readline() #Read nextt line
         
    
    
                
            
        
