#!/usr/bin/python3

######################################################## 
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import pandas as pd
import math
from Utility import *
from PENDFFileReader import *

class PENDFFile_MF6_inelastic(PENDFFileReader):
    
	def __init__(self, file_in_path, file_out_path, temperature):
		#print('PENDFFile_MF6_inelastic: file_in=', file_in_path)
		PENDFFileReader.__init__(self, file_in_path, 'dummy')
		#-------------------------------------------------------------
		# Read and transform PENDF data to G4NDL data
		#-------------------------------------------------------------        
		items = self.readLine() #header 1
		self.ZA = items[0]
		self.AWR = items[1]
		self.MF = items[7]
		self.MT = items[8]
		items = self.readLine() #header 2
		items = self.readLine() #header 3
		items = self.readLine() #header 4
		#print('In PENDFFile_MF6 Emin-Emax - items=', items)
		self.Emin = items[0]
		self.Emax = items[2]
		items = self.readLine() #header 5
		#print('In PENDFFile_MF6 temperature- items=', items)
		self.temperature = items[0]
		items = self.readLine() #header 6
		#print('PENDFFile_MF6_inelastic: items=', items, ' - file_in_path=', file_in_path)
		if items[0] == None:
			file_out = open(file_out_path, 'w')
			file_out.close()
			return 

		number_pair = int(items[0])
		
		isEOF = False 
		items = self.readLine() #header 7
		
		#---------------------------------------
		# Open output file
		#---------------------------------------
		print('-----------------------------------------')
		print('----------PENDFFile_MF6_inelastic--------')
		print('-----------------------------------------')
		file_out = open(file_out_path, 'w')
		
		file_out.write(formatString(self.MF)+'\n') 
		file_out.write(formatString(self.MT)+'\n') 
		file_out.write(formatString(temperature)+'\n') 
		file_out.write(formatString(number_pair)+'\n') 
		
		file_out.close()
		#file_out = open(file_out_path, 'ab')
		item_tmp = 0.
		NoneType = type(None)
		item_tmp_type = type(0.)
		item_type = type(0.)
		
		while not isEOF:
			E_Ep_mu = []
			data_line = []			
			
			#print('In PENDFFile_MF6 energy - items=', items)
			E = items[1]
			data_total_nb = items[4]
			block_size = int(items[5])
			iteration = 0
			block_size_tmp = 0

			#E_Ep_mu.append([0, E, 0, 0, int(data_total_nb), int(block_size), None, None, None, None])
			#print('============= MF6 inelastic ========')
			#print(f"E={E} -- block_size={block_size} -- data_total_nb={data_total_nb}")
			while iteration < data_total_nb:
				iteration = iteration + 6
				items = self.readLine()
				#print('iteration=', iteration, ' - data_total_nb=', data_total_nb)
				#print(f"\t items={items}")
				for j in range(6):
					
					##########################################################################################
					# --------------------- Correction version 1 ---------------------------------------------
					##########################################################################################
					# Correction apply on 2023/08/22
					# Temporary bug fix in NJOY: sometimes the last cos(theta) is small than the previous one...
					# This patch is here in waiting for final fix in NJOY
					# Vasilis Vlachoudis identifies the bug
					##########################################################################################
					#item_type = type(items[j])
					#if block_size_tmp > 2:
					#	if item_type!=NoneType and item_tmp_type!=NoneType:
					#		if item_tmp>items[j]:
					#			items[j] = min(item_tmp + 0.0000001, 0.9999999)
					##########################################################################################
					
					
					data_line.append( items[j] )
					
					block_size_tmp += 1

					if block_size_tmp==block_size:
						block_size_tmp = 0
						#print(f"data_line={data_line}")
						
						
						##########################################################################################
						# --------------------- Correction version 2 ---------------------------------------------
						##########################################################################################
						# Correction apply on 2023/08/22
						# Temporary bug fix in NJOY: sometimes the last cos(theta) is small than the previous one...
						# This patch is here in waiting for final fix in NJOY
						# Vasilis Vlachoudis identifies the bug
						##########################################################################################
						idx_max = block_size - 1
						isLastValue = False
						isModification = False
						pt_prev = -99.
						pt_curr = -99.
						pt_next = -99.
						#print(f"data_line={data_line} -- block_size={block_size}")
						for l in range(block_size-3):
							pt_prev = data_line[l+2]
							pt_curr = data_line[l+3]
						
							if l+4 <= idx_max:
								pt_next = data_line[l+4] 
							else:
								pt_next = -99.
								isLastValue = True			
								
							#print(f"read / idx={l+3}: pt_prev={pt_prev} -- pt_curr={pt_curr} -- pt_next={pt_next}")
						
							if (pt_curr < pt_prev) and (pt_curr < pt_next) and (isLastValue==False):
								#This means that the current point is locally lower than its neighbours...
								#print(f"Should not happen...")
								#input()
								data_line[l+3] = (pt_prev+pt_next)/2.
								#print(f"1/ update: pt_prev={pt_prev} -- pt_curr={pt_curr} -- pt_next={pt_next} ==> new value={data_line[l+3]}")
								#isModification = True
								
							elif (pt_curr < pt_prev) and (isLastValue==True):
								data_line[l+3] = data_line[l+2] + 0.0000001		
								#print(f"2/ update (last value): pt_prev={pt_prev} -- pt_curr={pt_curr} -- pt_next={pt_next} ==> new value={data_line[l+3]}")
								#isModification = True
							elif (pt_curr < pt_prev):
								#print(f"********************* Warning ************************")
								#print(f"data_line={data_line} -- block_size={block_size}")
								#print(f"Exclusive case: pt_curr={pt_curr} < pt_prev={pt_prev} ")
								data_line[l+3] = data_line[l+2] + 0.0000001	
								#print(f"0/ update (last value): pt_prev={pt_prev} -- pt_curr={pt_curr} -- pt_next={pt_next} ==> new value={data_line[l+3]}")
								isModification = True
								
																	
							#elif (pt_curr > pt_prev) and (pt_curr > pt_next) and (isLastValue==False):
							#	#This means that the current point is locally higher than its neighbours...
							#	pt_curr = (pt_prev+pt_next)/2.
							#	print(f"3.a/ update: pt_prev={pt_prev} -- pt_curr={data_line[l+3]} -- pt_next={pt_next} ==> new value={pt_curr}")
							#	if pt_curr < pt_prev:
							#		data_line[l+3] = data_line[l+2] + 0.0000001	
							#		isModification = True
							#		print(f"3.b/ update: pt_prev={pt_prev} -- pt_curr={pt_curr} -- pt_next={pt_next} ==> new value={data_line[l+3]}")
							#	else:
							#		print(f"Go to 1/ update")
						
						#if isModification == True:
						#	print(f"New dataline={data_line}")
						#	input()
						##########################################################################################
						
						E_Ep_mu.append( data_line )
						#print('-> data_line=', data_line)
						data_line = []
						item_tmp_type = type(0.)
						item_type = type(0.)
					
					item_tmp = items[j]
					item_tmp_type = item_type
			
			#Create a DataFrame
			colunm_names = ['Ep', 'pdf']
			for i in range(block_size-2):
				colunm_names.append('#mu'+str(i))

			df = pd.DataFrame(E_Ep_mu, columns=colunm_names)
			#print('df=', df)
			#input()
			#-------------------------------------------------------------
			# Write GNDL file
			#-------------------------------------------------------------
			#file_in = open(file_out_path+'_tmp', 'r')
			
			#file_out.write('{:<11}'.format('G4NDL')+'\n')
			#file_out.write('{:>11}'.format(ENDF_LIBRARY)+'\n')
						
			data_tmp = [0, E, 0, 0, int(data_total_nb), block_size]
			#print('data_tmp=', data_tmp)
			line = get_line_from_list_without_EOL(data_tmp) + '\n'

			#print('header=', line)

			file_out = open(file_out_path, 'a')
			file_out.write(line)
			#file_out.close()
			#np.savetxt(file_out, data_tmp, fmt='%.6e')
			#Write the data with a numpy array allowing to format the file
			values = df.values
			#print('values=', values)
			
			#values_2 = np.nan_to_num(values, nan=0, posinf=33333333, neginf=33333333)
			np.savetxt(file_out, values, fmt='%.6e') #correction apply on 2023/08/22, change FROM "np.savetxt(file_out, values, fmt='%.6e')" TO "np.savetxt(file_out, values, fmt='%.7e')"
			file_out.close()
			#input()
			items = self.readLine()
			
			#print('EOF? - items=', items)
			if None in items:
				#print('------- EOF -------')
				isEOF = True
				
		file_out.close()
